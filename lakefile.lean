import Lake
open Lake DSL

package «number_theory» {
  -- add package configuration options here
}

lean_lib «NumberTheory» {
  -- add library configuration options here
}

@[default_target]
lean_exe «number_theory» {
  root := `Main
}
