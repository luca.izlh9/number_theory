import NumberTheory.Div.ResidueClass

namespace Div

variable {d : Nat} {r : Fin d} {n : Nat}

def mod (d : Nat) (r : Fin d) (n : Nat) : Prop := ∃ k : Nat, n = d * k + r 

def divisible (d n : Nat) : Prop := ∃ k : Nat, n = d * k

def even (n : Nat) : Prop := divisible 2 n

def odd (n : Nat) : Prop := mod 2 1 n

theorem divisible_mod_lemma {d n : Nat} : divisible (Nat.succ d) n ↔ mod (Nat.succ d) 0 n := by
  apply Iff.intro
  case mp =>
    intro ⟨k, h₂⟩
    exact ⟨k, h₂⟩
  case mpr =>
    intro ⟨k, h₂⟩ 
    exact ⟨k, h₂⟩      

theorem mod_correct : mod d r n ↔ n % d = r := by
  apply Iff.intro
  case mpr =>
    intro hmod_eq_r
    --have hrltd : r < d := hmod_eq_r ▸ Nat.mod_lt n hdpos
    have : n = d * (n / d) + r := hmod_eq_r ▸ Eq.symm (Nat.div_add_mod n d)
    exact ⟨n / d, this⟩
  case mp =>
    induction n, d using Nat.div.inductionOn with
    | ind n d h₁ h₂ =>
      intro ⟨k, h₃⟩  
      have hdlen : d ≤ n := h₁.right
      apply (Nat.eq_zero_or_pos k).elim
      · intro hkeq0
        have : n = r := Nat.zero_add r ▸ Nat.mul_zero d ▸ hkeq0 ▸ h₃
        have : n < d := this ▸ r.isLt
        have : ¬d ≤ n := Nat.not_le_of_gt this
        contradiction
      · intro hkpos
        have hkne0 : k ≠ 0 := Nat.not_eq_zero_of_lt hkpos
        have : n - d = d * (k - 1) + r := calc
          n - d = d * k + r - d := by rw [h₃]
          _ = d * (Nat.succ (Nat.pred k)) + r - d := by rw [Nat.succ_pred hkne0]
          _ = d * (Nat.pred k) + d + r - d := by rw [Nat.mul_succ]
          _ = d * (Nat.pred k) + (r + d) - d := by rw [Nat.add_assoc, Nat.add_comm d r]
          _ = d * (k - 1) + (r + d) - d := rfl
          _ = (d * (k - 1) + (r + d)) - d := rfl
          _ = (d * (k - 1) + r + d) - d := by simp [Nat.add_assoc]
          _ = (d * (k - 1) + r) + d - d := rfl
          _ = d * (k - 1) + r := by simp [Nat.add_sub_cancel]
        have : mod d r (n - d) := ⟨k - 1, this⟩
        have : (n - d) % d = r := h₂ this
        exact (Nat.mod_eq_sub_mod hdlen) ▸ this
    | base n d h₁ =>
      intro ⟨k, hneqdk⟩
      have h₁ : d = 0 ∨ d > n := by
        apply Or.elim (Nat.eq_zero_or_pos d)
        · intro _
          exact Or.inl (by assumption)
        · intro hdpos
          apply Or.elim (Nat.lt_or_ge n d)
          · intro hnltd
            exact Or.inr hnltd
          · intro hnged
            exact absurd ⟨hdpos, hnged⟩ h₁
      apply h₁.elim
      · intro hdeq0
        have h₂ : n = r := calc
          n = d * k + r:= by rw [hneqdk]
          _ = 0 * k + r:= by simp [hdeq0]
          _ = r := by rw [Nat.zero_mul, Nat.zero_add]
        exact calc
          n % d = r % d := by rw [h₂]
          _ = r := by rw [Nat.mod_eq_of_lt r.isLt]
      · intro hdgtn
        apply Or.elim (Nat.eq_zero_or_pos k)
        · intro hkeq0
          have hneq0 : n = r := calc
            n = d * k + r:= hneqdk
            _ = r := by rw [hkeq0, Nat.mul_zero, Nat.zero_add]
          exact by rw [hneq0, Nat.mod_eq_of_lt r.isLt]
        · intro hkpos
          apply Or.elim (Nat.eq_zero_or_pos r)
          · intro heq0
            simp [heq0, Nat.add_zero] at hneqdk
            have : n * k < d * k := Nat.mul_lt_mul_of_pos_right hdgtn hkpos  
            have : n * k < n := hneqdk ▸ this
            have : n * Nat.succ (k - 1) < n := Nat.succ_pred (Nat.not_eq_zero_of_lt hkpos) ▸ this 
            have : n * (k - 1) + n < n := (Nat.mul_succ n (k - 1)) ▸ this
            have : n * (k - 1) < n - n := Nat.lt_sub_of_add_lt this
            have : n * (k - 1) < 0 := (Nat.sub_self n) ▸ this
            exact absurd this (Nat.not_lt_zero (n * (k - 1)))

          · intro _ 
            have : n * k < d * k := Nat.mul_lt_mul_of_pos_right hdgtn hkpos
            have : n * k + 0 < d * k + r := Nat.add_lt_add this ‹0 < r.val›   
            have : n * k < n := hneqdk ▸ this
            have : n * Nat.succ (k - 1) < n := Nat.succ_pred (Nat.not_eq_zero_of_lt hkpos) ▸ this 
            have : n * (k - 1) + n < n := (Nat.mul_succ n (k - 1)) ▸ this
            have : n * (k - 1) < n - n := Nat.lt_sub_of_add_lt this
            have : n * (k - 1) < 0 := (Nat.sub_self n) ▸ this
            exact absurd this (Nat.not_lt_zero (n * (k - 1)))

theorem mod_eq_of_modclass : mod d r n → n % d = r := by
  intro hmod
  exact mod_correct.mp hmod

theorem modclass_of_mod_eq : n % d = r → mod d r n := by
  intro hmod_eq_r
  exact mod_correct.mpr hmod_eq_r  

theorem divisible_iff_mod_zero {d n : Nat} : divisible d n ↔ n % d = 0 := by
  apply Iff.intro
  case mp =>
    intro ⟨k, h⟩ 
    cases Nat.eq_zero_or_pos d
    case inl heq =>
      simp [heq] at *
      assumption
    case inr hpos =>
      match d with
      | Nat.zero => contradiction
      | Nat.succ d =>
        have : mod (Nat.succ d) 0 n := divisible_mod_lemma.mp ⟨k, h⟩ 
        exact mod_eq_of_modclass this
  case mpr =>
    intro h
    cases d ; simp [Nat.mod_zero] at * ; exact ⟨0, by assumption⟩
    case succ d =>
      have : mod (Nat.succ d) 0 n := mod_correct.mpr h
      exact divisible_mod_lemma.mpr this

theorem mod_eq_zero_of_divisible {n d : Nat} : divisible d n → n % d = 0 := by
  intro h
  exact divisible_iff_mod_zero.mp h

theorem divisible_of_mod_eq_zero {n d : Nat} : n % d = 0 → divisible d n := by
  intros hmod0
  exact divisible_iff_mod_zero.mpr hmod0    

theorem div_eq_of_mul_eq {n d k : Nat} : 0 < d → n = d * k → n / d = k := by
  intros h₁ h₂
  have : n % d = 0 := mod_eq_zero_of_divisible ⟨k, h₂⟩ 
  have : n = d * (n / d) := calc
    n = d * (n / d) + n % d := Eq.symm (Nat.div_add_mod n d)
    _ = d * (n / d) := by rw [this, Nat.add_zero]
  have : d * k = d * (n / d) := h₂ ▸ this
  apply Eq.symm
  exact Nat.eq_of_mul_eq_mul_left h₁ this 

@[simp] theorem div_one (n : Nat) : n/1 = n := by
  exact Eq.symm (calc
    n = 1 * (n / 1) + n % 1 := by rw [Nat.div_add_mod]
    _ = n / 1 + n % 1 := by rw [Nat.one_mul]
    _ = n / 1 := by rw [Nat.mod_one, Nat.add_zero])

end Div