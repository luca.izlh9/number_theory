import NumberTheory.Div

open Div 

def pythagorean_triple (a b c : Nat) : Prop := 0 < a ∧ 0 < b ∧ a^2 + b^2 = c^2

structure PythagoreanTriple where
  a : Nat
  b : Nat
  c : Nat
  isPythagoreanTriple : pythagorean_triple a b c
  deriving Repr

namespace PythagoreanTriple

def toProd (pt : PythagoreanTriple) : Nat × Nat × Nat := (pt.a, pt.b, pt.c)

instance : Coe PythagoreanTriple (Nat × Nat × Nat) where
  coe := toProd  

def generate_pythagorean_triple (s t : Nat) : PythagoreanTriple :=
  let a : Nat := s * t
  let b : Nat := (s^2 - t^2) / 2
  let c : Nat := (s^2 + t^2) / 2
  ⟨a, b, c, sorry⟩  

#eval generate_pythagorean_triple 5 3

theorem generate_pythagorean_triple_is_correct (s t : Nat) : odd s → odd t → t < s → pythagorean_triple (s*t) ((s^2 - t^2) / 2) ((s^2 + t^2) / 2) := by
  intro ⟨k₁, hseq⟩ ⟨k₂, hteq⟩ hlt
  have : 0 < s * t := by
    have : 0 < 2 * k₂ + 1 := Nat.zero_lt_succ (2 * k₂)
    have : 0 < t := hteq ▸ this
    have : 0 * t < s * t := Nat.mul_lt_mul_of_pos_right (Nat.zero_lt_of_lt hlt) this
    simp at this
    assumption
  simp [pythagorean_triple]
  apply And.intro this
  
       

end PythagoreanTriple