import NumberTheory.Pow
namespace Square

open Pow

def square (n : Nat) : Prop := ∃ k : Nat, n = k^2

theorem square_of_sum (a b : Nat) : (a + b)^2 = a^2 + 2 * a * b + b^2 := by
  have : a*b*2 = 2*a*b := calc
    a*b*2 = a*(b*2) := by simp [Nat.mul_assoc]
    _ = a*(2*b) := by simp [Nat.mul_comm]
    _ = 2*(a*b) := by simp [Nat.mul_left_comm]
    _ = 2*a*b := by simp [Nat.mul_assoc]
  exact this ▸ calc
    (a + b)^2 = a*a + b*a + (a*b + b*b) := by simp [pow_two, Nat.left_distrib, Nat.right_distrib]
    _ = a^2 + a*b + (a*b + b^2) := by simp [pow_two, Nat.mul_comm]
    _ = a^2 + (a*b + a*b + b^2) := by simp [Nat.add_assoc]
    _ = a^2 + ((a*b*1 + a*b) + b^2) := by simp
    _ = a^2 + (a*b*2 + b^2) := by simp [Nat.mul_succ]
    _ = a^2 + a*b*2 + b^2 := by simp [<-Nat.add_assoc]

--why is this not in the standard library?
theorem sub_brackets (n : Nat) {m k : Nat} : k ≤ m → n - (m - k) = n + k - m := by
  induction k with
  | zero    =>
    intros
    exact calc
      n - (m - 0) = n - m := by simp
      _ = n + 0 - m := by simp
  | succ k' h =>
    intro h₂
    have : n - (m - k') = n + k' - m := h (Nat.le_of_succ_le h₂)
    have : _ := calc 
      n - (m - Nat.succ k') = n - Nat.pred (m - k') := by simp [Nat.sub_succ]
      _ = Nat.succ (n - (m - k')) := sorry
    sorry

theorem square_of_diff {a b : Nat} : b ≤ a → (a - b)^2 = a^2 + b^2 - 2 * a * b := by
  intro hblea
  have : (a - b)^2 = _ := calc
    (a - b)^2 = (a - b) * (a - b) := by simp 
    _ = (a - b) * a - (a - b) * b := by simp [Nat.mul_sub_left_distrib]
    _ = a * a - b * a - (a * b - b * b) := by simp [Nat.mul_sub_right_distrib]
    _ = a^2 - b * a - (a * b - b^2) := by simp
    _ = a^2 - a * b - (a * b - b^2) := by simp [Nat.mul_comm]
    _ = a^2 - a * b - a * b + b^2 := sorry
  sorry

theorem diff_of_squares (a b : Nat) : a^2 - b^2 = (a + b) * (a - b) := by
  apply Eq.symm
  exact calc
    (a + b) * (a - b) = (a + b) * a - (a + b) * b := by simp [Nat.mul_sub_left_distrib]
    _ = a * a + b * a - (a * b + b * b) := by simp [Nat.right_distrib]
    _ = a^2 + b * a - (a * b + b^2) := by simp
    _ = a^2 + a * b - (a * b + b^2) := by simp [Nat.mul_comm]
    _ = a^2 + a * b - a * b - b^2 := by simp [Nat.sub_add_eq]
    _ = (a^2 + a * b - a * b) - b^2 := by simp
    _ = (a^2 + (a * b - a * b)) - b^2 := by simp [Nat.add_sub_assoc]
    _ = a^2 - b^2 := by simp 

theorem square_identity (n : Nat) : square (n^2) := by exact ⟨n, rfl⟩  

theorem square_of_product_of_squares {n a b : Nat} : n = a^2 * b^2 → square n := by
  intro h
  apply Exists.intro (a * b)
  exact calc
    n = a^2 * b^2 := h
    _ = a * a * (b * b) := by simp
    _ = a * (a * (b * b)) := by simp [Nat.mul_assoc]
    _ = a * (b * (a * b)) := by simp [Nat.mul_left_comm]
    _ = a * b * (a * b) := by simp [Nat.mul_assoc]
    _ = (a * b) * (a * b) := by simp
    _ = (a * b)^2 := by simp

theorem product_of_squares_of_square {n a b : Nat} : n^2 = a * b → square a ∧ square b := by
  sorry

end Square