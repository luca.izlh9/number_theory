structure ResidueClass (d : Nat) (r : Fin d) where
  k : Nat
  deriving Repr

namespace ResidueClass

variable {d : Nat}
variable {r : Fin d}

def toNat : ResidueClass d r → Nat
 | mk k => d*k + r

instance : Coe (ResidueClass d r) Nat where
  coe := toNat

theorem res_cls_of_eq {n : Nat} {m : ResidueClass d r} : n = m → n = d * m.k + r := by
  intro h 
  match m with
  | mk k => 
    simp [toNat] at *
    assumption

def ofNat (n : Nat) : ResidueClass d r := mk (n / d)

instance (n : Nat) : OfNat (ResidueClass d r) n where
  ofNat := ofNat n

theorem ofNat_toNat (n : ResidueClass d r) : ofNat (toNat n) = n := by
  cases n 
  case mk k => 
    simp [toNat, ofNat]
    induction k with
    | zero => 
        simp [Nat.div_eq (r.val) d]
        split 
        case zero.inl h => 
          have : r.val < d := r.isLt
          have : ¬d ≤ r.val := Nat.not_le_of_gt this
          have : d ≤ r.val := h.right
          contradiction
        case zero.inr h => rfl
    | succ k ih =>
        rw [calc
          (d * Nat.succ k + r.val) / d = (d * k + d + r.val) / d := by simp [Nat.mul_succ]
          _ = (d * k + (d + r.val)) / d := by simp [Nat.add_assoc]
          _ = (d * k + (r.val + d)) / d := by simp [Nat.add_comm]]
        rw [Nat.div_eq]
        split
        · rw [calc
          (d * k + (r.val + d) - d) / d + 1 = (d * k + r.val + d - d) / d + 1 := by rw [Nat.add_assoc (d*k) r.val d]
          _ = (d * k + r.val + (d - d)) / d + 1 := by rw [Nat.add_sub_assoc (Nat.le_refl d)]
          _ = (d * k + r.val + 0) / d + 1 := by rw [Nat.sub_self]
          _ = (d * k + r.val) / d + 1 := by rw [Nat.add_zero (d*k + r.val)]]
          rw [ih]
        · have : d ≤ d*k + (r.val + d) := 
            have h₁ : d ≤ r.val + d := Nat.le_add_left d r.val
            have h₂ : r.val + d ≤ d*k + (r.val + d) := Nat.le_add_left (r.val + d) (d*k)
            Nat.le_trans h₁ h₂
          have hpos : 0 < d := by
            apply (Nat.eq_zero_or_pos d).elim
            case left h => 
              intro heq
              have : r.val < 0 := heq ▸ r.isLt 
              have : ¬r.val < 0 := Nat.not_lt_zero r
              contradiction
            case right _ => 
              intro 
              assumption 
          have := And.intro hpos this
          contradiction

theorem toNat_ofNat {d : Nat} {r : Fin d} {n : Nat} : n % d = r → toNat (@ofNat d r n) = n := by
  induction n, d using Nat.div.inductionOn with
  | ind n d h ih =>
    intro h₂
    have : n % d = (n - d) % d := by
      simp [Nat.mod_eq n d]
      split 
      case inl => rfl
      case inr => contradiction
    have : (n - d) % d = r.val := this ▸ h₂
    have h₃ : toNat (ofNat (n - d)) = n - d := ih this
    simp [toNat, ofNat] at *
    have : n / d = (n - d) / d + 1 := by
      simp [Nat.div_eq n d]
      split
      case inl => rfl
      case inr => contradiction
    simp [this, Nat.mul_succ, Nat.add_assoc, Nat.add_comm d r.val]
    have : d * ((n - d) / d) + (r.val + d) = d * ((n - d) / d) + r.val + d := by rw [Nat.add_assoc]
    simp [this]
    have := Nat.eq_add_of_sub_eq h.right (Eq.symm h₃)
    have := Eq.symm this
    assumption
  | base n d h => 
    intro h₂
    have : 0 < d := by
      apply (Nat.eq_zero_or_pos d).elim
      case left h => 
        intro heq
        have : r.val < 0 := heq ▸ r.isLt 
        have : ¬r.val < 0 := Nat.not_lt_zero r
        contradiction
      case right => intro ; assumption
    have : ¬0 < d ∨ ¬d ≤ n := Iff.mp (Decidable.not_and_iff_or_not ..) h
    have : ¬d ≤ n := by
      cases this
      contradiction
      assumption
    have : n < d := by cases (Nat.lt_or_ge n d) ; assumption ; contradiction
    simp [toNat, ofNat]
    have : n % d = n := by
      simp [Nat.mod_eq n d]
      split ; contradiction ; rfl
    have : n = r.val := this ▸ h₂
    simp [Nat.div_eq n d]
    split ; contradiction ; simp ; apply Eq.symm ; assumption

def Even : Type := ResidueClass 2 0

instance : Coe Even (ResidueClass 2 0) where
  coe := id

instance (n : Nat) : OfNat Even n where
  ofNat := ofNat n

def Odd : Type := ResidueClass 2 1

instance : Coe Odd (ResidueClass 2 1) where
  coe := id

instance (n : Nat) : OfNat Odd n where
  ofNat := ofNat n
